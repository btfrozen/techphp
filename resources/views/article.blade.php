@extends('layout')



@section('body')

    <div class="page_content">

        <section class="site-main">
            <div class="blog-post">

                <article id="post-36"
                         class="single-post post-36 post type-post status-publish format-standard hentry category-uncategorized">
                    <header class="entry-header">
                        <h1 class="single_title">{{  $article->article->title}}</h1>

                    </header><!-- .entry-header -->

                    <div class="postmeta">
                        <div class="post-date">   </div><!-- post-date -->
                       
                        <div class="clear"></div>
                    </div><!-- postmeta -->
                    @foreach($article->body as $row)

                       @if ($row->row_type == "createds")
                        <p>{{$row->createds()}}</p>
                        @endif
                        @if ($row->row_type == "image")
                        <img src="{{$row->getImage()->src}}" srcset="{{$row->getImage()->srcset}}"/>
                        @endif

                      

                        @if ($row->row_type == "text")
                        <p>{{$row->text()}}</p>
                        @endif

                        @if ($row->row_type == "h2")
                        <h2>{{$row->text()}}</h2>
                        @endif
                    @endforeach
                    <footer  style="text-align: left;" class="entry-meta">
                        <span>Дата публикации:{{$date_post}}</span>
                    </footer><!-- .entry-meta -->

                </article>


                <div class="relatePost">
                    <h5>Похожие записи</h5>
                    @foreach($related as $late)
                    <div class="itemrelated">
                        <div class="boxe">
                        <a href="/{{ $late->url  }}">
                            <img src="{{ $late->image->src}}"
                                     class="attachment-medium size-medium wp-post-image" alt=""
                                     srcset="{{ $late->image->srcset }}" sizes="(max-width: 100px) 100vw, 100px">
                                 </a>
                                 <a class="namer" href="/{{ $late->url  }}">{{ $late->title }}</a>
                              </div></div>
                              @endforeach

                </div>


            </div>

        </section>

            <section class="newPost">
        <h4>Новое на сайте</h4>
         @foreach($lates as $late)
         <div class="itemsd">
             <a href="/{{ $late->url  }}">
                 <img src="{{ $late->image->src}}"
                                     class="attachment-medium size-medium wp-post-image" alt=""
                                     srcset="{{ $late->image->srcset }}" sizes="(max-width: 100px) 100vw, 100px">
             </a>
             <a class="namer" href="/{{ $late->url  }}">{{ $late->title }}</a>
         </div>

         @endforeach

    </section>
    </div><!-- site-aligner -->
    
</div><!-- content -->
<div style="clear: both;"></div>
<footer>
    Все права защищены. © 2019 <a href="/rulles/" >Политика конфиденциальности</a>  <noindex><a rel="nofollow" href="/sitemap/">Карта сайта</a></noindex>
</footer>

@endsection