<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   {!! SEOMeta::generate() !!}
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel="stylesheet" id="wp-block-library-css"
          href="https://wp-themes.com/wp/wp-includes/css/dist/block-library/style.min.css?ver=5.2-alpha-45015"
          type="text/css" media="all">
    <link rel="stylesheet" id="herbal-lite-font-css"
          href="//fonts.googleapis.com/css?family=Roboto+Condensed%3A300%2C400%2C600%2C700%2C800%2C900%7CRoboto+Slab%3A300%2C400%2C700%7CMerriweather%3A300%2C400%2C400i%2C700%2C700i%2C900%2C900i%7CRoboto%3A300%2C300i%2C400%2C400i%2C500%2C500i%2C700%2C700i%2C900%2C900i%7CLato%3A300%2C300i%2C400%2C400i%2C700%2C700i%2C900%2C900i&amp;ver=5.2-alpha-45015"
          type="text/css" media="all">
    <link rel="stylesheet" id="herbal-lite-basic-style-css"
          href="https://wp-themes.com/wp-content/themes/herbal-lite/style.css?ver=5.2-alpha-45015" type="text/css"
          media="all">
    <link rel="stylesheet" id="herbal-lite-print-style-css"
          href="https://wp-themes.com/wp-content/themes/herbal-lite/print.css?ver=5.2-alpha-45015" type="text/css"
          media="all">
    <link rel="stylesheet" id="nivo-slider-css"
          href="https://wp-themes.com/wp-content/themes/herbal-lite/css/nivo-slider.css?ver=5.2-alpha-45015"
          type="text/css" media="all">
    <link rel="stylesheet" id="font-awesome-css"
          href="https://wp-themes.com/wp-content/themes/herbal-lite/css/font-awesome.css?ver=5.2-alpha-45015"
          type="text/css" media="all">
    <link rel="stylesheet" id="herbal-lite-main-style-css"
          href="https://wp-themes.com/wp-content/themes/herbal-lite/css/responsive.css?ver=5.2-alpha-45015"
          type="text/css" media="all">
    <link rel="stylesheet" id="herbal-lite-base-style-css"
          href="https://wp-themes.com/wp-content/themes/herbal-lite/css/style_base.css?ver=5.2-alpha-45015"
          type="text/css" media="all">
    <link rel="stylesheet" id="herbal-lite-custom-style-css"
          href="https://wp-themes.com/wp-content/themes/herbal-lite/css/custom_script.css?ver=5.2-alpha-45015"
          type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <style id="herbal-lite-custom-style-inline-css" type="text/css">

        #sidebar ul li a:hover,
        .threebox:hover h3,
        .cols-3 ul li a:hover, .cols-3 ul li.current_page_item a,
        .phone-no strong,
        .left a:hover,
        .blog_lists h4 a:hover,
        .recent-post h6 a:hover,
        .postmeta a:hover,
        .recent-post .morebtn:hover {
            /*color: ;*/
        }

        .copyright-wrapper, .pagination .nav-links span.current, .pagination .nav-links a:hover,
        #commentform input#submit:hover,
        .slide_info .slide_more:hover,
        .wpcf7 input[type='submit'],
        .social-icons a:hover,
        a.ReadMore, .benefitbox-4:hover .benefitbox-title,
        input.search-submit {
            /*background-color: ;*/
        }

        .head-info-area {
            /*border-top-color: ;*/
        }

        h3.widget-title:after {
            /*border-bottom-color: ;*/
        }

        .benefitbox-4:hover .benefit-thumb {
            /*border-color: ;*/
        }

        .benefitbox-4:hover .benefitbox-title:after {
            border-bottom-color: ;
        }


        #menubar,
        h2.section-title::after,
        h2.section-title {
            border-color: ;
        }

        .logo h2, .logo p {
            color: #000000;
        }

    </style>
{{--    <script type="text/javascript" src="https://wp-themes.com/wp/wp-includes/js/jquery/jquery.js?ver=1.12.4"></script>--}}
{{--    <script type="text/javascript"--}}
{{--            src="https://wp-themes.com/wp/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1"></script>--}}
{{--    <script type="text/javascript"--}}
{{--            src="https://wp-themes.com/wp-content/themes/herbal-lite/js/jquery.nivo.slider.js?ver=5.2-alpha-45015"></script>--}}
{{--    <script type="text/javascript"--}}
{{--            src="https://wp-themes.com/wp-content/themes/herbal-lite/js/custom.js?ver=5.2-alpha-45015"></script>--}}
    <meta name="yandex-verification" content="6491c0a34c9e0129" />
    <meta name="google-site-verification" content="LrWuqvK-huz4ommU3v3Fc384qwvLgI_6Bb2ISK248zQ" />
</head>
<body class="home blog">
<div class="header">
    <div class="container">
        <div class="logo">
            <div class="clear"></div>
            <noindex>
            <a href="/" rel="nofollow">
                <h2>Ofoz<span>Ru</span></h2>
                <p><br></p>
            </a>
           </noindex>
        </div>
        <!--
        <div class="toggle"><a class="toggleMenu" href="#" style="display:none;">Menu</a></div>
        <div class="sitenav" style="display: block;">
            <div class="menu">
                <ul>
                    <li class="current_page_item"><a href="https://wp-themes.com/">Home</a></li>
                    <li class="page_item page-item-2"><a href="https://wp-themes.com/?page_id=2">About</a></li>
                    <li class="page_item page-item-46 page_item_has_children"><a
                                href="https://wp-themes.com/?page_id=46" class="parent">Parent Page</a>
                        <ul class="children">
                            <li class="page_item page-item-49"><a href="https://wp-themes.com/?page_id=49">Sub-page</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

        </div> -->
        <div class="clear"></div>
    </div> <!-- container -->
</div><!--.header -->
<div class="clear"></div>
<div class="container">

    @yield('body')

    <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
            (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
                m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
            (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

            ym(53555620, "init", {
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true
            });
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/53555620" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->

</body>
</html>
