@extends('layout')


@section('body')

<div class="page_content">

    <section class="site-main">
        <div class="blog-post">


            @foreach($articles as $article)

                <div class="blog_lists">
                    <article class="post type-post status-publish format-standard hentry category-uncategorized">

                        <div class="article-preview-image">
                            <a href="/{{ $article->url  }}">

                                <img src="{{ $article->image->src}}"
                                     class="attachment-medium size-medium wp-post-image" alt=""
                                     srcset="{{ $article->image->srcset }}" sizes="(max-width: 300px) 100vw, 300px">
                            </a>
                        </div>

                        <header class="entry-header">
                            <h4><a href="/{{ $article->url  }}" rel="bookmark">{{ $article->title }}</a></h4>
                            <div class="postmeta">
                                <div class="post-date">{{ $article->created }}</div><!-- post-date -->
                               
                            </div><!-- postmeta -->
                        </header><!-- .entry-header -->

                        <div class="entry-summary">
                            <noindex>
                            <p>{{ $article ->previewText }}</p>
                            </noindex>
                            <div class="clear"></div>
                            <p><a class="ReadMore" href="/{{ $article->url  }}">Подробнее</a></p>
                        </div><!-- .entry-summary -->
                        <div class="clear"></div>
                    </article><!-- #post-## -->
                </div>
            @endforeach
        </div>

        <div class="pagination">

            <ul>
                @if ($pagination->prev() != 0)
                    <li><a href="?page={{ $pagination->prev() }}"> &lt;&lt; </a></li> @endif
                @foreach($pagination->pages() as $page)

                    <li><a href="?page={{ $page }}"
                           @if ($pagination->page == $page) class="current" @endif >{{ $page}}</a></li>

                @endforeach
                @if ($pagination->next() != 0)
                    <li><a href="?page={{ $pagination->next() }}">&gt;&gt;</a></li> @endif
            </ul>
        </div>

    </section>

    <section class="newPost">


    </section>
   
    <div class="clear"></div>
</div><!-- site-aligner -->


</div><!-- content -->
<div style="clear: both;"></div>
<footer>
    Все права защищены. © 2019 <a href="/rulles/" >Политика конфиденциальности</a> <a href="/sitemap/">Карта сайта</a>
</footer>
@endsection