<?php

namespace App;

use App\DataClass\Image;
use Illuminate\Database\Eloquent\Model;

class Body extends Model
{
    protected $table = 'Body';


    function text(): string {
        return $this->text_ru;
    }
   

    function getImage(): Image {
        return Image::fromJson($this->text_en);
    }
    //
}
