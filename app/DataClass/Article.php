<?php

namespace App\DataClass;
use Carbon\Carbon;


class Article {
    /**
     * @var int
     */
    public $id;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $previewText;
    /**
     * @var string
     */
    public $created;

    /**
     * @var Image
     */
    public $image;
    public $url;


    public function __construct(int $id, string $title, string $previewText, Image $image)
    {
        $this->id = $id;
        $this->title = $title;
        $this->url = $this->transliterate($title);
        $this->previewText = $previewText;
        $this->image = $image;
     }

    static function fromRow($row): Article {
        return new Article($row->id, $row->title_ru, $row->preview_ru, Image::fromJson($row->preview_image));
    }

public function transliterate($input){
$gost = array(
"а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d",
"е"=>"e", "ё"=>"yo","ж"=>"j","з"=>"z","и"=>"i",
"й"=>"i","к"=>"k","л"=>"l", "м"=>"m","н"=>"n",
"о"=>"o","п"=>"p","р"=>"r","с"=>"s","т"=>"t",
"у"=>"y","ф"=>"f","х"=>"h","ц"=>"c","ч"=>"ch",
"ш"=>"sh","щ"=>"sh","ы"=>"i","э"=>"e","ю"=>"u",
"я"=>"ya","Ф"=>"F","Х"=>"H","Ц"=>"C","Ч"=>"Ch",
"Ш"=>"Sh","Щ"=>"Sh","Ы"=>"I","Э"=>"E","Ю"=>"U",
"Я"=>"Ya",
"ь"=>"","Ь"=>"","ъ"=>"","Ъ"=>"",
"ї"=>"j","і"=>"i","ґ"=>"g","є"=>"ye",
"Ї"=>"J","І"=>"I","Ґ"=>"G","Є"=>"YE"
);
$str=strtr($input, $gost);

    $str = str_replace(' ', '-', $str);
   
    $str = trim($str, "-");
    $str = mb_strtolower($str);



return $str;
}





}

class Image {

    /**
     * @var string
     */
    public $src;
    /**
     * @var string
     */
    public $srcset;

    public function __construct(string $src, string $srcset)
    {
        $this->src = $src;
        $this->srcset = $srcset;
    }

    static function fromJson(string $s): Image {
        $rs = json_decode($s);
        return new Image($rs->src, $rs->srcset);
    }

    public function __toString()
    {
        return "Image($this->src, $this->srcset)";
    }
}


