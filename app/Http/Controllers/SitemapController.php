<?php
namespace App\Http\Controllers;

use App\Body;
use App\DataClass\Article;
use App\DataClass\Image;
use App\Pages;
use SEO;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class SitemapController extends Controller {

    function index() {

    	SEO::setTitle('Карта сайта');

    	$lates = DB::table('Pages')->orderByRaw('created_at DESC')->get();
    	$oust = [];
        foreach ($lates as $rows) {
            $oust[]=Article::fromRow($rows); //->id, $row->title_en, $row->preview_en, Image::fromJson($row->preview_image));
        }


    	return view('sitemap',
            [
            	'oust'=>$oust
            ]);

    }

    function rulles(){
    	SEO::setTitle('Политика конфиденциальности');
    	return view('rulles');
    }

    function sitemap(){
    	 $posts = DB::table('Pages')->orderByRaw('created_at DESC')->get();
$oust = [];
        foreach ($posts as $rows) {
            $oust[]=Article::fromRow($rows); //->id, $row->title_en, $row->preview_en, Image::fromJson($row->preview_image));
        }

        return response()->view('sitemaps', compact('oust'))->header('Content-Type', 'text/xml');
 


          
    }


public function transliterate($input){
$gost = array(
"а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d",
"е"=>"e", "ё"=>"yo","ж"=>"j","з"=>"z","и"=>"i",
"й"=>"i","к"=>"k","л"=>"l", "м"=>"m","н"=>"n",
"о"=>"o","п"=>"p","р"=>"r","с"=>"s","т"=>"t",
"у"=>"y","ф"=>"f","х"=>"h","ц"=>"c","ч"=>"ch",
"ш"=>"sh","щ"=>"sh","ы"=>"i","э"=>"e","ю"=>"u",
"я"=>"ya","Ф"=>"F","Х"=>"H","Ц"=>"C","Ч"=>"Ch",
"Ш"=>"Sh","Щ"=>"Sh","Ы"=>"I","Э"=>"E","Ю"=>"U",
"Я"=>"Ya",
"ь"=>"","Ь"=>"","ъ"=>"","Ъ"=>"",
"ї"=>"j","і"=>"i","ґ"=>"g","є"=>"ye",
"Ї"=>"J","І"=>"I","Ґ"=>"G","Є"=>"YE"
);
$str=strtr($input, $gost);

    $str = str_replace(' ', '-', $str);
   
    $str = trim($str, "-");
    $str = mb_strtolower($str);



return $str;
}

}