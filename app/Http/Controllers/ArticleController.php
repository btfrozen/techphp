<?php
namespace App\Http\Controllers;

use App\Body;
use App\DataClass\Article;
use App\DataClass\Image;
use App\Pages;
use SEO;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller {

    function index() {

        $data="2019-05-01";
        $now = time();
        $your_date = strtotime($data);
        $datediff = $now - $your_date;
        $limit=round($datediff / (60 * 60 * 24))*20;
 

        SEO::setTitle('Главная');
        SEO::setDescription('Ofoz');
        $cnt =$limit; // $cnt = Pages::query()->count();
        $p = new Pagination($cnt , (int)Input::get("page"), 10, 5);
        $rs = Pages::query()->limit(10)->offset($p->offset)->get();

        $out = [];
        foreach ($rs as $row) {
            $getss = Article::fromRow($row);
            $dates= DB::table('Pages')->where('id','=',$row->id)->get();
            $getss->created=str_replace('+00:00', ' ',str_replace('T', ' ', $dates[0]->created_at));
            $out[]=$getss; //->id, $row->title_en, $row->preview_en, Image::fromJson($row->preview_image));
        }


        $lates = DB::table('Pages')->limit(10)->orderByRaw('created_at DESC')->get();
        $oust = [];
        foreach ($lates as $rows) {
           // print_r($rows->created_at);
            $gets = Article::fromRow($rows);
            $gets->created=$rows->created_at;
             
            $oust[]=$gets; //->id, $row->title_en, $row->preview_en, Image::fromJson($row->preview_image));
        }

  
 

        return view('index',
            ['articles'=>new Paginator($out, 10, Input::get("page")),
                'pagination'=>$p,
                'lates'=>$oust
            ]);
    }

    function article(string $id) {

 
        $chety = DB::table('Pages')->get();
        foreach ($chety as $key => $value) {
            $he=$this->transliterate($value->title_ru);
             if($he==$id){
                $id=$value->id;
                break;
            }
        }

        


 
        $p = Pages::findOrFail($id);

        $body = Body::query()->where("page_id", "=", $p->id)->get();
        $liker =explode(' ', $p->title_ru);



 
        $new_posts = DB::table('Pages')->limit(3)->orderByRaw('created_at DESC')->get();
        $related_posts = DB::table('Pages')->where('id', '!=', $p->id);
        foreach ($liker as $key => $value) {
            $related_posts = $related_posts->orWhere('title_ru', 'LIKE', '%'.$value.'%');
        }
        $related_posts = $related_posts->inRandomOrder()->limit(9)->get();

        //->inRandomOrder()
        $oust = [];
        foreach ($new_posts as $rows) {
            $oust[]=Article::fromRow($rows); //->id, $row->title_en, $row->preview_en, Image::fromJson($row->preview_image));
        }
 
        $ousts = [];
        foreach ($related_posts as $rowss) {
            $ousts[]=Article::fromRow($rowss); //->id, $row->title_en, $row->preview_en, Image::fromJson($row->preview_image));
        }

 
        SEO::setTitle($p->title_ru);
        SEO::setDescription($p->preview_ru);
        $infoPage = DB::table('Pages')->where('id','=',$p->id)->get();

        $url =$this->transliterate($infoPage[0]->title_ru);

 
        $date_post = str_replace('+00:00', ' ',str_replace('T', ' ', $infoPage[0]->created_at));
 
        return view('article',
            [
                'lates'=>$oust,
                'date_post'=>$date_post,
                'related'=>$ousts,
                'article' => new ArticleWithBody(Article::fromRow($p), $body),
            ]);
    }






 


public function transliterate($input){
$gost = array(
"а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d",
"е"=>"e", "ё"=>"yo","ж"=>"j","з"=>"z","и"=>"i",
"й"=>"i","к"=>"k","л"=>"l", "м"=>"m","н"=>"n",
"о"=>"o","п"=>"p","р"=>"r","с"=>"s","т"=>"t",
"у"=>"y","ф"=>"f","х"=>"h","ц"=>"c","ч"=>"ch",
"ш"=>"sh","щ"=>"sh","ы"=>"i","э"=>"e","ю"=>"u",
"я"=>"ya","Ф"=>"F","Х"=>"H","Ц"=>"C","Ч"=>"Ch",
"Ш"=>"Sh","Щ"=>"Sh","Ы"=>"I","Э"=>"E","Ю"=>"U",
"Я"=>"Ya",
"ь"=>"","Ь"=>"","ъ"=>"","Ъ"=>"",
"ї"=>"j","і"=>"i","ґ"=>"g","є"=>"ye",
"Ї"=>"J","І"=>"I","Ґ"=>"G","Є"=>"YE"
);
$str=strtr($input, $gost);

    $str = str_replace(' ', '-', $str);
   
    $str = trim($str, "-");
    $str = mb_strtolower($str);



return $str;
}




}


class ArticleWithBody {
    /**
     * @var Article
     */
    public $article;
    /**
     * @var array
     */
    public $body;

    public function __construct(Article $article, iterable $body)
    {

        $this->article = $article;
        $this->body = $body;
    }
}


class Pagination {

    /**
     * @var int
     */
    private $total;
    /**
     * @var int
     */
    public $page;
    /**
     * @var int
     */
    private $perPage;
    /**
     * @var int
     */
    private $pagesShown;

    public $min;
    public $max;

    public function __construct(int $total, int $page, int $perPage = 10, int $pagesShown= 5)
    {
        $this->total = $total;
        $this->page = $page;
        $this->perPage = $perPage;
        $this->pagesShown = $pagesShown;

        $this->min = 1;

        if ($total % $perPage == 0) {
            $this->max = $total / $perPage;
        } else {
            $this->max = $total / $perPage + 1;
        }

        $this->limit = $perPage;
        $this->offset = ($page - 1) * $perPage;

        if ($this->page < 0) {
            $this->page = 1;
        }
        if ($this->page > $this->max) {
            $this->page = $this->max;
        }

        $this->pagesShown = min($pagesShown, ($this->max + 1) - $this->min);
    }

    function pages(): array /*: List<Int>*/ {
        return $this->_pages($this->pagesShown, $this->page - intval($this->pagesShown / 2), $this->page + intval($this->pagesShown / 2));
    }

    private function _pages(int $cnt, int $min, int $max): array {
        if ($min < $this->min) {
            return $this->_pages($cnt, $min + 1, $max + 1);
        }

        if ($max > $this->max) {
            return $this->_pages($cnt, $min - 1, $max - 1);
        }

        if (($max + 1) - $min != $cnt) {
            return $this->_pages($cnt, $min, $max - 1);
        }

        $out = [];
        $i = $min;
        while ($i < $max + 1) {
            $out[] = $i;
            $i++;
        }

        return $out;
    }

    public function prev(): int {
        if ($this->page > $this->min) {
            return $this->page - 1;
        }
        return 0;
    }

    public function next(): int {
        if ( $this->page < $this->max) {
            return $this->page + 1;
        }
        return 0;
    }


}
