<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ArticleController@index');
Route::get('sitemap.xml', 'SitemapController@sitemap');
Route::get('/sitemap/', 'SitemapController@index');
Route::get('/rulles/', 'SitemapController@rulles');
Route::get('/{id}', 'ArticleController@article');

//
//Route::get('/', function () {
//    return view('index');
//});
